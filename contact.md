---
layout: default
regenerate: true
title: Contact Us
index_title: Contact
---


<h3>Contact us</h3>
<p>
    If you would like to contact the LUG, you can do so in several ways:
</p>
<table>
    <td>
    <h6>Cougsync &amp; Social Media</h6>
    <ul>
        <li>Facebook: <a href="https://www.facebook.com/groups/291665697579415/">WSU LUG</a></li>
        <li><a href="https://orgsync.com/40157/chapter">Cougsync</a></li>
    </ul>
    </td>
    <td>
    <h6>Slack</h6>
    <ul>
        <li>Signup Here: <a href="https://join.slack.com/t/wsulug/shared_invite/enQtNzg1MDMzOTEzODQ0LTJkNTJhM2RjNDZkMWUzNjRkNGY2ZWEwZTM5MDI1OWE3Y2ZjZWJhZmFlOWExMTAyYzA0ODQ3NGYwY2M2ZTI1MDk">Invite Link</a></li>
        <li>Once you've signed up: <a href="https://slack.com/downloads">download the app</a> or visit the <a href="https://wsulug.slack.com">web app</a></li>
    </ul>
    </td>
    <td>
    <h6>Internet Relay Chat (IRC)</h6>
    <ul>
        <li>#wsulug @ freenode.net</li>
        <li><a href="/irc.html">Connect to #wsulug via our website</a></li>
    </ul>
    </td>
</table>
<h6>Mailing Lists:</h6>
<p>
    We have a few different mailing lists that we use to communicate events and other happenings with club members. If you would like to receive updates from list, please subscribe to one.
</p>
<ul>
    <li>lug-announce (<a href="http://lists.wsu.edu/mailman/listinfo/lug-announce">Subscribe</a>): We send meeting, event, and other announcements here. If in doubt, this is your list!</li>
    <li>lug-general (<a href="http://lists.wsu.edu/mailman/listinfo/lug-general">Subscribe</a>): General discussions as well as support requests</li>
    <li>lug-alumni (<a href="http://lists.wsu.edu/mailman/listinfo/lug-alumni">Subscribe</a>): Low volume list for alumni to communicate with one another, or for the LUG to send out alumni announcements</li>
</ul>

You can unsubscribe at any time.

<h6>Email:</h6>
<p>
    If none of the other options work, please send us an email @ wsulugadmin@gmail.com
</p>

<div class="column two">


