---
layout: default
regenerate: true
title: Meetings
index_title: Meetings
---

# Meetings
			

### Meeting Minutes

You can find all of our meeting minutes <a href="https://drive.google.com/folderview?id=0BzETrghxI1LJWkdpbGl5Tl9qT2s&usp=sharing">here.</a>

### Upcoming Meetings

<iframe src="https://calendar.google.com/calendar/embed?showTitle=0&amp;showNav=0&amp;showDate=0&amp;showPrint=0&amp;showTabs=0&amp;showCalendars=0&amp;showTz=0&amp;mode=AGENDA&amp;height=600&amp;wkst=1&amp;bgcolor=%23999999&amp;src=qvd0rfqek0tkped06v549b3f3o%40group.calendar.google.com&amp;color=%23125A12&amp;ctz=America%2FLos_Angeles" style="border-width:0" width="100%" height="500px" frameborder="0" scrolling="no"></iframe>
