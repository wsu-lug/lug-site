---
layout: default
regenerate: true
title: Linux Users' Group at Washington State University
index_title: LUG Home
---
<p class="move-down">
<big>The Washington State Linux Users Group (LUG)</big> exists to promote the visibility and use of Linux and other Free and Open Source software. We give away free Linux CDs and Open Source software CDs for other operating systems, help install Linux, and run mailing lists providing free Linux support. It can be a big jump to break away from proprietary software, so we're here to help.
</p>
<h3>What We Do</h3>
<p>Our goal is to give students opportunities in both academia and the professional world in
order to further their knowledge and experience. To do this, we provide:</p>
<ul>
	<li><strong>Linux Help &amp; Advice:</strong> Offered during tutoring and office hours</li>
	<li><strong>Tutoring:</strong> First four Saturdays of each semester, 11am-4pm, free pizza and drinks. See our <a href="/meetings.html">meetings/tutoring page</a> for more details.</li>
	<li><strong>Tech Talks:</strong> <em>Roughly</em> every other week, please check our <a
href="/meetings.html">meetings</a> page for more information</li>
	<li><strong>Two Yearly LAN Parties:</strong> WinterWonderLAN in the Fall, <a
href="http://wsucon.wsu.edu">WSUCon</a> in the Spring</li>
</ul>

<h3>How to Join</h3>
<p>Joining the LUG is easy, all you have to do is show up to meetups, eat pizza, and
participate. We welcome everyone and would love to have you involved in our club. If you
aren't sure when/where our meetings are, just check back on this website or join our
Facebook group for updates on our meetings and other events. If you are looking to get more
involved, please check out our <a href="/get-involved.html">Get Involved!</a> page for more
information.</p>

<h3>LUG Drive</h3>
<p>In order to keep our club open and transparent to all members, we have an open access Google
Drive where we have our constitution, meeting minutes, proposals, and other important
information. You can check it out <a
href="https://drive.google.com/folderview?id=0BzETrghxI1LJWkdpbGl5Tl9qT2s&usp=sharing">here.</a>



<h3>Officers</h3>
<p>President - Charlie Hanacek<br />
Vice President - [unfilled]<br />
Treasurer - Michael Sinclair<br />
Secretary - Daniel Zimmer<br />
Sysadmin Lead - Alex Askerman</p>

