---
layout: default
regenerate: true
title: Pi Condo
index_title: Raspberry Pi Condo
---
<h3>Pi Condo</h3>
<p class="move-down">
	The Pi Condo project is an initiative by the LUG to give members a place for their Raspberry Pi (or other ARM-based 
	single-board computer). The Pi Condo is a reserved space inside our server rack with shelving to hold devices and supply them
	with up to 2.4 Amps over a 5v USB 2.0, Gigabit Ethernet, a <em>fast</em> fiber-optic connection. <br />
	
	<h4><em>The Pi Condo is live as of May 2018!</em></h4>
</p>
<p>
	If you are interested in getting your device in the Condo, join the #picondo channel on our <a href="https://lugslack.eecs.wsu.edu">Slack</a>.
	<br />
	
</p>

<h4>Custom Baseplates</h4>
<p>
	When a Pi is installed in the Condo, it is physically attached to an 1/8th inch acrylic baseplate (usually with zip ties). The LUG does not provide baseplates, but we do provide the pins to connect your baseplate to the shelf. To cut a baseplate, you can usually find plenty of scrap material in the EECS Fiz (Frank Innovation Zone) in the basement of Dana.
	<br />

	You may change anything within the bounds of the baseplate, except for the location of the holes for the pins. Those need to remain intact so that we can ensure fair space usage within the Condo as well as compatibility with our mounting system. Please note that the units for the dxf files are millimeters, not inches.
	<br />

</p>
<p>
	Here is the vector file for the full-sized Pi baseplate: <a href="https://drive.google.com/file/d/1ys9A5a8egaoRvhAICOi_WSUbOrj7MSKp/view?usp=sharing">Full Sized Baseplate</a>
	<br />
</p>
<p>
	Here is the vector file for the Pi Zero baseplate: <a href="https://drive.google.com/open?id=1qunU8Lew4dd2poE6TcbO3RDyaXvGShxA">Pi Zero Baseplate</a>

</p>

<h4>Community Agreement</h4>
<p>
	The Pi Condo exists to promote the educational use of Linux for students at WSU. We ask that you sign a community agreement that shows your commitment to this goal. Bring it with you when you bring your Pi to be installed in the Condo.
</p>

<br />
<p>
	The community agreement can be found here: <a href="https://drive.google.com/open?id=1CG_A63ZC_7fLf40qAeObAp7co5woYGI3">Pi Condo Community Agreement</a>
</p>
