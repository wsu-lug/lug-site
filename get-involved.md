---
layout: default
regenerate: true
title: Get Involved
index_title: Get Involved
---
<p class="move-down"><big>Are you interested in getting involved with the LUG?</big> Great! This is the perfect place for you.</p>
<p>
  There are a few ways and levels of involvement when it comes to the LUG, which makes getting involved quite easy.
</p>
<h5>Member</h5>
<p>
  Becoming a member in the LUG is very easy, it simply requires showing up to meetings and events during the year. Being a member is easy, fun, and involves lots of free food.
</p>
<h5>Event Organizer</h5>
<p>
  We host a few large events during the year that require a great deal of help from outside of our small officer group. Getting involved with helping out with an event is a great way to become an officer. This option also involves even more free food!
</p>
<p>
  One of the largest events that we frequently need help with is our LAN party held in the spring, WSUCon. There are a few different levels within our WSUCon planning committee that allow people to get involved in based on how busy their schedule is:
</p>
<ul>
  <li>Full Organizer: Shows up to all WSUCon planning meetings, is assigned different tasks and expected to complete them on time and be active on our WSUCon planning chat</li>
  <li>Tournament Organizer: Shows up to all WSUCon planning meetings where tournament organizers are required at and must keep up with the chat when tournaments are being discussed. Most work for tournaments will be done on the day of the event, but there will be some logistical stuff that needs to be worked out beforehand.</li>
  <li>Volunteer: A volunteer is only required to attend a handfull of all organizer meetings, volunteers must keep up with the chat to know what is going on. Volunteers must let the head organizer know their availability for WSUCon so they can schedule them for various tasks. Some volunteer tasks include:</li>
  <ul>
    <li>Tournament Organizer Assistant</li>
    <li>Registration Table Helper</li>
    <li>Food Helper (must have a food handler's permit)</li>
    <li>General Volunteer (helping people setup and bring their things in)</li>
  </ul>
</ul>
<h5>Being an Officer</h5>
<p>
  Being an officer of the LUG is a great way to get involved outside of class. We elect new officers through a very democratic process in the spring semester each year. If you are interested in becoming an officer, please attend meetings and keep up with the club so that you will know when our officer elections will happen. We elect officers for the following positions every spring semester:
</p>
<ul>
  <li>President</li>
  <li>Vice President</li>
  <li>Treasurer</li>
  <li>Secretary</li>
  <li>Sysadmin</li>
</ul>
<p>
  To learn more about our officer positions, you can check out our <a href="https://drive.google.com/file/d/0BzETrghxI1LJTkJoeWR6dUlKSEU/view?usp=sharing">constitution.</a> You can also come to some of our office hours and speak to officers about their experiences and why they became an officer of the LUG.
</p>
