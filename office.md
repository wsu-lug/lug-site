---
layout: default
regenerate: true
title: Our Office
index_title: Our Office
---

### Office Hours
Starting October 16th, 2019 we will be hosting office hours on the following schedule:

Mondays 5:00pm-6:30pm  
Tuesdays 5:00pm-6:30pm  
Wednesdays 5:00pm-6:30pm  

Office hours are held in our beloved tiny space in Dana 216(B). There are many signs throughout Dana with arrows indicating the relative location of the LUG office to help you find your way if you get lost. If office hours get changed last-minute, we'll notify people on the #general channel on Slack.

During office hours, the office is open! We have bright lights, monitors so that you can extend your screen, and a very fast internet connection. We also have a long workbench if you prefer a standing desk. Stop by and say hi!

### Office Location
Dana 216(B)
NE Spokane St  
Pullman, WA 99163  

#### 2019 Officers

##### President  
**Charlie Hanacek**  
charlie.hanacek@wsu.edu  

##### Vice President
**Vacant**

##### Treasurer
**Michael Sinclair**  
michael.sinclair@wsu.edu

##### Secretary  
**Daniel Zimmer**  
daniel.zimmer@wsu.edu

##### SysAdmin  
**Alex Askerman**  
alex.askerman@wsu.edu  