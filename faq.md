---
layout: default
regenerate: true
title: FAQs
index_title: FAQs
---

### Frequently Asked Questions

##### How do I become a member?

Becoming a member is easy, all  you have to do is show up to meetings and events.

##### When and where are meetings held?

Meetings are announced on our <a href="https://www.facebook.com/groups/291665697579415/">Facebook page</a> as well as here on our <a href="/meetings.html">meetings page.</a>

##### How do I become an officer?

Please visit our <a href="/get-involved.html">Get Involved!</a> page for more information on how to get involved.

##### I missed a meeting, what do I do?

Check out our <a href="/meetings.html">meetings page</a> and visit our <a href="https://drive.google.com/folderview?id=0BzETrghxI1LJWkdpbGl5Tl9qT2s&usp=sharing">Google Drive</a> for meeting minutes and other important information that you might have missed.


##### How do I get in contact with the LUG officers?

You can contact us via our <a href="/contact.html">Contact Us</a> page. Please only contact us with specific questions that can't be answered from our website or social media.

##### What if my question wasn't answered here?

Please email us at wsulugadmin@gmail.com with your question and we will get back to you as soon as we can.
